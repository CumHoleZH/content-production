# Social media content plan

https://about.gitlab.com/handbook/social-media/ 

### Approach for @ GitLab account

-   Chatty: Friendly, conversational with the rest of the community. 
-   Promote: Help promote contributors, users and ideas that our users will appreciate. 
-   Curate: Promote information about related topics such as open source, git, devops, etc.
-   Responsive: Direct support requests to @GitLabSupport 

### Twitter accounts

-   @GitLabStatus - Managed by service engineers
    -   Outage, announcements about service
-   @GitLabSupport - Managed by service engineers
    -   Possible to break out service support to another account? 
-   @GitLab -  Managed by marketing team
    -   GitLab news: share our content. 
        -   Links to blog posts, Events we’ll be presenting at, news from our staff people, nice things our folks are working on. 
    -   GitLab community: share content from community.
        -   Projects hosted on GitLab, any wins from GitLab clients, quotes from and links to blog posts about GitLab.
    -   Curated content: share content that reflects our views and values and interests. 
        -   Quotes from books we’re reading, links to great  blog posts about open organisations, version control, collaboration. 

### Scenarios

- Someone mentions @ GitLab with a link to a blog post they wrote. 
    - Reply to and Thank author! 
    - Make original tweet (not RT) Clip out quote from blog post, share link and @mention the author. 
- Someone mentions @ GitLab with a complaint about an outage
    - Handover to @GitLabStatus, who will respond
- Someone mentions @ GitLab with a feature request
    - Direct them to contribute an issue or vote on feature
- Somone requests support @ GitLab
    - Then @ GitLab mentions @GitLabSupport in a hanover
    - @GitLabSupport is tracked in ZenDesk
    - Service Engineer team coordinate the response via @GitLabSupport
    - @nearlythere to coordinate w patricio re: zendesk and gitlabsupport