## Research about other groups

# Examples of user groups

This is an overview of some software community user groups. 

### Dogfood

Some communities eat their own dogfood, if their application relies on community.
* Drupal uses Drupal http://groups.drupal.org
* Salesforce uses their own software:  Salesforce Communities

### Decentralized

Some software communities have a decentralized approach. 
This means the local community can use whatever tool to organize their event online.
Usually this is Meetup.com

Alfresco uses Wiki to organise, but the local groups use whatever they like. Such as Meetup.com

* [Example: Alfreso](https://wiki.alfresco.com/wiki/Local_Communities#Atlanta_User_Group)

[RedHat community events](http://community.redhat.com/events/)

* Community members self-organise via Meetup.com - But central info for community events page is submitted via GitHub. 
* https://github.com/OSAS/rh-events/wiki/Adding-and-modifying-events
* E.g., if you want to contribute an event, you do so on GitHub by forking > editing a .yml file and then creating a PR. 

## Centralized

Atlassian - They use SplashThat, a commercial service. 

* AUG Atlassianabout "10,000 people take part in Atlassian user groups (AUGs) in more than 25 countries."
* Atlassian funds pizza, drink, venue, etc for user groups.
* "AUG leader" group leaders apply via a form - and they must schedule 4 meetups per year.  http://www.surveygizmo.com/s3/1656259/I-want-to-be-an-Atlassian-User-Group-Leader
* Those interested indicate interest via a form https://web.archive.org/web/20140911070954/http://www.surveygizmo.com/s3/1794131/New-City-Prospectus
* Atlassian have three staff focused on community management
* SplashThat - 499/event - 249/month  or enterprise 999/month https://splashthat.com/packages

## Somewhere in between

Docker seems to hit a sweet spot between centralized support; but decentralized effort. 
Nice ideas from the Docker team http://www.slideshare.net/julienbarbier42/community-management-best-practices-docker?from_action=save

* Community member applies to start a group > Docker staff person creates the meetup.com group. https://www.docker.com/starting-docker-meetup-group
* https://www.docker.com/community Google map of Meet-up groups.
* List of groups on Meetup.com https://www.docker.com/starting-docker-meetup-group
* They list other places you can connect to Docker - Reddit, LinkedIn, Facebook, YouTube, etc
* Potential members can sign up "As a Meetup organizer / As a blogger / As a speaker / As a User / Customer Reference / As a sponsor”  https://docker.mobilize.io/entities/3177/registrations
* They use Mobilize.io for organising community contribution. https://docker.mobilize.io/